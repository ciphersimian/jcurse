package org.bitbucket.keiki.jcurse.console;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.bitbucket.keiki.jcurse.console.Console.main;
import static org.junit.Assert.assertTrue;

public class AddAddonIT {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();
    private File folder;

    @Before
    public void beforeEach() throws IOException {
        folder = tempFolder.newFolder();
        main("--config", Paths.get(folder.getAbsolutePath(), "config").toString(), "--set-wow", folder.getAbsolutePath());
    }

    @Test
    public void addAddonBagnon() throws IOException {
        addAddon("bagnon", "Bagnon");
    }

    @Test
    public void addAddonAltoholic() throws IOException {
        addAddon("altoholic", "Altoholic");
    }

    @Test
    public void addAddonWeakauras2() throws IOException {
        addAddon("weakauras-2", "WeakAuras");
    }

    private void addAddon(String addonName, String expectedDirectoryName) {
        main("--config", Paths.get(folder.getAbsolutePath(), "config").toString(), "remove", addonName);

        main("--config", Paths.get(folder.getAbsolutePath(), "config").toString(), "add", addonName);

        Path addonDirectory = Paths.get(folder.getAbsolutePath(), "Interface", "AddOns", expectedDirectoryName);
        assertTrue(Files.exists(addonDirectory));
    }
}
