package org.bitbucket.keiki.jcurse.console;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.bitbucket.keiki.jcurse.AddonFileHandlerMock;
import org.bitbucket.keiki.jcurse.AddonRepoPersistenceMock;
import org.bitbucket.keiki.jcurse.AddonInstallationManager;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;

public class ConsoleTest {

    @Rule
    public final StandardOutputStreamLog log = new StandardOutputStreamLog();
    
    @Test (expected = BusinessException.class)
    public void testFewArguments() {
        Console.executeArguments(Arrays.asList(new String[0]));
    }
    
    private boolean isInstalled(AddonRepoPersistenceMock arpm, String name, int version)
    {
        boolean isInstalled = false;
        for(Addon addon : arpm.loadInstalledAddons())
            if(addon.getAddonNameId().equals(name) && addon.getVersionId() == version)
                return true;
        return false;
    }

    @Test
    public void testAdd() {
        AddonRepoPersistenceMock arpm = new AddonRepoPersistenceMock(true);
        AddonFileHandlerMock afhm = new AddonFileHandlerMock();
        AddonInstallationManager repositoryManager = new AddonInstallationManager(arpm, afhm);
        
        assertFalse(isInstalled(arpm, "bagnon", 63476));

        Console.executeCommands(Arrays.asList("add","bagnon"), repositoryManager, "add");
        
        assertTrue(isInstalled(arpm, "bagnon", 63476));
    }
    
    @Test
    public void testRemove() {
        AddonRepoPersistenceMock arpm = new AddonRepoPersistenceMock(true);
        AddonFileHandlerMock afhm = new AddonFileHandlerMock();
        AddonInstallationManager repositoryManager = new AddonInstallationManager(arpm, afhm);

        assertTrue(isInstalled(arpm, "test1", 2477989));

        Console.executeCommands(Arrays.asList("remove","test1"), repositoryManager, "remove");
        
        assertFalse(isInstalled(arpm, "test1", 2477989));
    }
    
    @Test
    public void testUpdateAll() {
        AddonRepoPersistenceMock arpm = new AddonRepoPersistenceMock(true);
        AddonFileHandlerMock afhm = new AddonFileHandlerMock();
        AddonInstallationManager repositoryManager = new AddonInstallationManager(arpm, afhm);

        assertTrue(isInstalled(arpm, "test1", 2477989));
        assertTrue(isInstalled(arpm, "test2", 1054));

        Console.executeCommands(Arrays.asList("update","all"), repositoryManager, "update");

        assertTrue(isInstalled(arpm, "test1", 2477989));
        assertTrue(isInstalled(arpm, "test2", 1055));
    }
    
    @Test
    public void testUpdateSingle() {
        AddonRepoPersistenceMock arpm = new AddonRepoPersistenceMock(true);
        AddonFileHandlerMock afhm = new AddonFileHandlerMock();
        AddonInstallationManager repositoryManager = new AddonInstallationManager(arpm, afhm);

        assertTrue(isInstalled(arpm, "test2", 1054));

        Console.executeCommands(Arrays.asList("update","test2"), repositoryManager, "update");

        assertTrue(isInstalled(arpm, "test2", 1055));
    }
    
    @Test
    public void testList() {
        AddonInstallationManager repositoryManager = new AddonInstallationManager(new AddonRepoPersistenceMock(true),
                new AddonFileHandlerMock());
        Console.executeCommands(Arrays.asList("list"), repositoryManager, "list");
        String result = log.getLog() != null? log.getLog() : "";
        assertTrue(result.contains("Currently installed addons:"));
        assertTrue(result.contains("test1"));
        assertTrue(result.contains("test2"));
    }
    
    @Test
    public void testListNoAddon() {
        AddonInstallationManager repositoryManager = new AddonInstallationManager(new AddonRepoPersistenceMock(false),
                new AddonFileHandlerMock());
        Console.executeCommands(Arrays.asList("list"), repositoryManager, "list");
        String result = log.getLog() != null? log.getLog() : "";
        assertTrue(result.contains("We don't know of any installed addon."));
    }
    
    @Test
    public void testExport() {
        AddonInstallationManager repositoryManager = new AddonInstallationManager(new AddonRepoPersistenceMock(true),
                new AddonFileHandlerMock());
        Console.executeCommands(Arrays.asList("export"), repositoryManager, "export");
        String result = log.getLog() != null? log.getLog() : "";
        assertTrue(result.contains("jcurse add test1 test2"));
    }
    
    @Test
    public void testExportNoAddon() {
        AddonInstallationManager repositoryManager = new AddonInstallationManager(new AddonRepoPersistenceMock(false),
                new AddonFileHandlerMock());
        Console.executeCommands(Arrays.asList("export"), repositoryManager, "export");
        String result = log.getLog() != null? log.getLog() : "";
        assertTrue(result.contains("No addon(s) are installed"));
    }
    
    @Test (expected = BusinessException.class)
    public void unknownCommandOneArg() {
        AddonInstallationManager repositoryManager = new AddonInstallationManager(new AddonRepoPersistenceMock(false),
                new AddonFileHandlerMock());
        Console.executeCommands(Arrays.asList("lis"), repositoryManager, "lis");
    }
    
    @Test (expected = BusinessException.class)
    public void unknownCommandTwoArg() {
        AddonInstallationManager repositoryManager = new AddonInstallationManager(new AddonRepoPersistenceMock(false),
                new AddonFileHandlerMock());
        Console.executeCommands(Arrays.asList("ad", "two"), repositoryManager, "ad");
    }
}
