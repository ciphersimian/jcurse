package org.bitbucket.keiki.jcurse;

import java.util.Collection;
import java.util.List;

import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.io.Curse;

public class AddonFileHandlerMock implements Curse {

    @Override
    public void removeAddons(Collection<Addon> toDelete) {
    }

    @Override
    public List<Addon> downloadToWow(List<Addon> toDownload) {
        if (!toDownload.isEmpty() && toDownload.get(0).getAddonNameId().equals("unknownAddon")) {
            return toDownload.subList(1, toDownload.size());
        }
        for(Addon addon : toDownload)
            downloadToWow(addon, "");
        return toDownload;
    }

    @Override
    public void downloadToWow(Addon addon, String downloadUrl) {
        switch(addon.getAddonNameId())
        {
        case "test1":
            addon.setVersionId(2477989);
            break;
        case "test2":
            addon.setVersionId(1055);
            break;
        case "bagnon":
            addon.setVersionId(63476);
            break;
        case "buxtehude":
            addon.setVersionId(12345);
            break;
        case "bus1":
            addon.setVersionId(54321);
            break;
        case "bus2":
            addon.setVersionId(1);
            break;
        default:
            assert false : "Unknown addon: " + addon.getAddonNameId();
        }
    }

    @Override
    public String getDownloadUrl(Addon addon) {
        switch(addon.getAddonNameId())
        {
        case "test1":
            return "http://localhost/projects/test1/files/2477989/download";
        case "test2":
            return "http://localhost/projects/test2/files/1055/download";
        case "bagnon":
            return "http://localhost/projects/bagnon/files/63476/download";
        case "buxtehude":
            return "http://localhost/projects/buxtehude/files/12345/download";
        case "bus1":
            return "http://localhost/projects/bus1/files/54321/download";
        case "bus2":
            return "http://localhost/projects/bus2/files/1/download";
        }
        assert false : "Unknown addon: " + addon.getAddonNameId();
        return "";
    }

    @Override
    public void removeAddon(Addon toDelete) {
        // TODO Auto-generated method stub
        
    }

}
