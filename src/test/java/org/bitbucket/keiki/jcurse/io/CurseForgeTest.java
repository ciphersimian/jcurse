package org.bitbucket.keiki.jcurse.io;

import static org.junit.Assert.assertEquals;

import static org.bitbucket.keiki.jcurse.io.Constants.CHARSET_WEBSITE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.NoSuchElementException;

import java.util.Optional;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.ReleaseStatus;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForge;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForgeCommunication;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForgeFactory;
import org.bitbucket.keiki.jcurse.io.downloadurl.UrlGetter;
import org.junit.Test;

public class CurseForgeTest {
    
    private Addon addon = Addon.newInstance(Arrays.asList("dunesimplebuffs"), ReleaseStatus.BETA).get(0);

    @Test
    public void successfulGetDownloadUrl() {
        CurseForgeCommunicationTestable comm = new CurseForgeCommunicationTestable();
        UrlGetter curseForge = CurseForgeFactory.Create(addon, comm);
        
        String downloadUrl = curseForge.getDownloadUrl().get();
        
        assertEquals("http://wow.curseforge.com/projects/dunesimplebuffs/files/797148/download", downloadUrl);
    }
    
    @Test
    public void getDownloadUrlBeta() {
        CurseForgeCommunicationTestableMissingLine comm = new CurseForgeCommunicationTestableMissingLine();
        UrlGetter curseForge = CurseForgeFactory.Create(addon, comm);
        assertEquals(Optional.empty(), curseForge.getDownloadUrl());
    }
    
    private class CurseForgeCommunicationTestable extends CurseForgeCommunication {
        @Override
        public int executeHttp(HttpClient httpClient, GetMethod method) throws IOException, HttpException {
            return 200;
        }

        @Override
        public BufferedReader getStreamOverviewSite(GetMethod method) throws IOException {
            return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("curseForgeOverview.html"), CHARSET_WEBSITE));
        }

        @Override
        public BufferedReader getStreamDetailsSite(GetMethod method) throws IOException {
            return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("curseForgeDetail.html"), CHARSET_WEBSITE));
        }
    }

    private class CurseForgeCommunicationTestableMissingLine extends CurseForgeCommunicationTestable {
        @Override
        public BufferedReader getStreamOverviewSite(GetMethod method) throws IOException {
            return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("curseForgeOverview2ndLineMissing.html"), CHARSET_WEBSITE));
        }
    }
}
