#!/usr/bin/env bash

find . -name "*.java" | xargs javac -classpath "$CLASSPATH"
