package org.bitbucket.keiki.jcurse.console;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.SystemUtils;
import org.bitbucket.keiki.jcurse.AddonInstallationManager;
import org.bitbucket.keiki.jcurse.Configuration;
import org.bitbucket.keiki.jcurse.ConfigurationImpl;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.bitbucket.keiki.jcurse.data.ReleaseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class Console {

    private static final int TWO_ARGS = 2;

    private static final String SET_WOW_ARGUMENT = "--set-wow";

    private static final String CONFIG_ARGUMENT = "--config";

    private static final Logger LOG = LoggerFactory.getLogger(Console.class);
    
    private Console() {
        // main class
    }
    
    public static void main(String... args) {
        if (SystemUtils.IS_OS_WINDOWS) {
            LOG.error("This application is not allowed to run on windows. Use the official Curse client.");
            return;
        }
        try {
            executeArguments(Arrays.asList(args));
        } catch (BusinessException e) {
            LOG.error(e.getMessage());
            LOG.debug("Somethings wrong. Known exception.", e);
            LOG.info("\r\nUsage:");
            LOG.info("jcurse add (alpha|beta) [addon name1, name2, ...]");
            LOG.info("jcurse remove [addon name1, name2, ...]");
            LOG.info("jcurse update (--force|-f) [addon name1, name2, ... | all]");
            LOG.info("jcurse set [alpha|beta|release] [addon name, ...]");
            LOG.info("jcurse list");
            LOG.info("jcurse export");
            LOG.info("jcurse " + SET_WOW_ARGUMENT + " <full path to wow folder>");
            LOG.info("jcurse " + CONFIG_ARGUMENT + " <full path to alternate config to use> (default: " + ConfigurationImpl.DEFAULT_CONFIG_FILE_LOCATION + ")");

        }
    }

    static void executeArguments(List<String> arguments) {
        if (arguments.isEmpty()) {
            throw new BusinessException("Number of arguments are wrong");
        }
        List<String> changeableArgs = new ArrayList<String>(arguments);
        Configuration config = executeConfigChanges(changeableArgs);
        if(changeableArgs.isEmpty())
            return;
        config.load();
        String command = changeableArgs.get(0);
        executeCommands(changeableArgs, new AddonInstallationManager(config), command);
    }

    static void executeCommands(List<String> arguments,
            AddonInstallationManager repositoryManager, String command) {
        executeOneArgumentCommand(arguments, repositoryManager, command);
        executeTwoArgumentsCommand(arguments, repositoryManager, command);
    }

    /**
     * Looks for the specified arg in args, and if it finds it, returns the
     * parameter of the arg (the next entry in List).  If the arg is not found
     * an empty string is returned.  If arg exists in the List it will be
     * removed (along with whatever follows it).
     */
    private static String checkForAndRemoveArg(String arg, List<String> args)
    {
        List<String> remainingArgs = new ArrayList<>();
        String param = "";
        for (int i = 0; i < args.size(); i++) {
            if(args.get(i).equals(arg))
            {
                if (args.size()-1 == i)
                {
                    BusinessException be = new BusinessException("Expected arg after: "+args.get(i));
                    LOG.debug("Exception in checkForAndRemoveArg: {}", be.stackTraceString());
                    throw be;
                }
                else
                {
                    param = args.get(++i);
                }
            }
            else
            {
                remainingArgs.add(args.get(i));
            }
        }
        args.clear();
        args.addAll(remainingArgs);
        return param;
    }

    /**
     * Processes and removes any config related arguments and then returns the
     * remaining arguments to process.
     */
    private static Configuration executeConfigChanges(List<String> args) {
        Configuration config = null;

        // This method only operates on two argument options which begin with
        // --; it will process any --config argument first, where ever it is
        // in the command line, and any --set-wow argument second, if there is
        // one.
        String param = checkForAndRemoveArg(CONFIG_ARGUMENT, args);
        if (!param.isEmpty()) {
            config = new ConfigurationImpl(param);
            LOG.info("Using alternate config directory " + param);
        }
        else
        {
            config = new ConfigurationImpl();
        }

        param = checkForAndRemoveArg(SET_WOW_ARGUMENT, args);
        if (!param.isEmpty()) {
            config.setWowFolder(param);
            config.save();
            LOG.info("Changed wow directory to " + param);
        }

        return config;
    }

    private static void executeTwoArgumentsCommand(List<String> arguments,
            AddonInstallationManager repositoryManager, String command) {
        if (arguments.size() < TWO_ARGS) {
            return;
        }
        List<String> unprocessedArgs = arguments.subList(1, arguments.size());
        switch (command) {
            case "add":
                add(repositoryManager, unprocessedArgs);
                break;
            case "remove":
                repositoryManager.remove(unprocessedArgs);
                break;
            case "update":
                update(repositoryManager, unprocessedArgs);
                break;
            case "set":
                setReleaseStatus(repositoryManager, unprocessedArgs);
                break;
            default:
                throw new BusinessException("Unrecognized command " + command);
        }
    }

    private static void setReleaseStatus(AddonInstallationManager repositoryManager, List<String> unprocessedArgs) {
        ReleaseStatus status = ReleaseStatus.valueOfIgnoreCase(unprocessedArgs.get(0));
        repositoryManager.setReleaseStatus(status, unprocessedArgs.subList(1, unprocessedArgs.size()));
    }

    private static void add(
            AddonInstallationManager repositoryManager,
            List<String> unprocessedArgs) {
        ReleaseStatus status = null;
        List<String> subList = null;
        try
        {
            status = ReleaseStatus.valueOfIgnoreCase(unprocessedArgs.get(0));
            subList = unprocessedArgs.subList(1, unprocessedArgs.size());
        } catch (IllegalArgumentException e) {
            status = ReleaseStatus.RELEASE;
            subList = unprocessedArgs;
        }
        
        List<Addon> added = repositoryManager.add(subList, status);
    }

    private static void update(AddonInstallationManager repositoryManager, List<String> unprocessedArgsPara) {
        List<String> unprocessedArgs = unprocessedArgsPara;
        String secondParameter = unprocessedArgs.get(0);
        boolean forceUpdate = false;
        if ("-f".equalsIgnoreCase(secondParameter) || "--force".equalsIgnoreCase(secondParameter)) {
            forceUpdate = true;
            LOG.info("Force update!");
            unprocessedArgs = unprocessedArgs.subList(1, unprocessedArgs.size());
        }
        if ("all".equalsIgnoreCase(unprocessedArgs.get(0))) {
            LOG.info("updating all addons");
            repositoryManager.updateAll(forceUpdate);
        } else {
            repositoryManager.update(unprocessedArgs, forceUpdate);
        }
        LOG.info("all addons are now up2date");
    }

    private static void executeOneArgumentCommand(List<String> arguments,
            AddonInstallationManager repositoryManager, String command) {
        if (arguments.size() != 1) {
            return;
        }
        switch (command) {
            case "list":
                listAddons(repositoryManager.getAddons(), false);
                break;
            case "listv":
                listAddons(repositoryManager.getAddons(), true);
                break;
            case "export":
                exportAddons(repositoryManager.getAddons());
                break;
            default:
                throw new BusinessException("Unregonized command " + command);
        }
    }

    private static void exportAddons(Collection<Addon> addons) {
        if (addons.isEmpty()) {
            LOG.info("No addon(s) are installed");
            return;
        }
        StringBuilder build = new StringBuilder("jcurse add");
        for (Addon addon : addons) {
            build.append(' ').append(addon.getAddonNameId());
        }
        LOG.info(build.toString());
    }

    private static void listAddons(Collection<Addon> addons, boolean verbose) {
        if (addons.isEmpty()) {
            LOG.info("We don't know of any installed addon.");
            return;
        }
        LOG.info("Currently installed addons:");
        
        for (Addon addon : addons) {
            if (verbose) {
                LOG.info(addon.toStringVerbose());
            } else {
                LOG.info(addon.toString());
            }
        }
    }
}
