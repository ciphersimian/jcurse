package org.bitbucket.keiki.jcurse.data;

import java.io.StringWriter;
import java.io.PrintWriter;

public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BusinessException(String message, Exception ex) {
        super(message, ex);
    }
    
    public BusinessException(String message) {
        super(message);
    }

    public String stackTraceString()
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        printStackTrace(pw);
        return sw.toString();
    }
} 
