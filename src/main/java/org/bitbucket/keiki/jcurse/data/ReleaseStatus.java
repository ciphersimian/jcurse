package org.bitbucket.keiki.jcurse.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ReleaseStatus {
    ALPHA, BETA, RELEASE;

    private static final Logger LOG = LoggerFactory.getLogger(ReleaseStatus.class);
    
    public static ReleaseStatus valueOfIgnoreCase(String status) {
        return valueOf(status.toUpperCase());
    }
}
