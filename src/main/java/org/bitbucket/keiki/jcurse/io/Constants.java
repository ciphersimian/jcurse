package org.bitbucket.keiki.jcurse.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public final class Constants {
    public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.75 Chrome/62.0.3202.75 Safari/537.36";
    public static final Charset CHARSET_WEBSITE = StandardCharsets.UTF_8;

    private Constants() {
    }
}
