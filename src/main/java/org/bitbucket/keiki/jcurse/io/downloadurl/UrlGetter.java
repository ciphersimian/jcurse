package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.util.Optional;

public interface UrlGetter {

    Optional<String> getDownloadUrl();
}
