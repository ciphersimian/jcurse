package org.bitbucket.keiki.jcurse.io.downloadurl;

import static org.bitbucket.keiki.jcurse.io.Constants.USER_AGENT;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.util.Optional;
import org.apache.commons.httpclient.methods.GetMethod;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.bitbucket.keiki.jcurse.data.ReleaseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CurseForge provides addons in two different but very similar ways; projects and
 * wow/addons pages.  The reason for this is mostly historical as many addons
 * used to be hosted on wow.curse.com and mods.curse.com but curse.com is now
 * purely a corporate site and all the addon hosting has moved to one of two
 * URL formats:
 *
 * http://wow.curseforge.com/projects/<addon>
 *
 * OR
 *
 * http://www.curseforge.com/wow/addons/<addon>
 *
 * Some addons have both while others work in only one or the other.  This
 * makes life a royal pain in the ass for us.
 *
 * These sites are similar enough that we attempt to handle both paths with
 * one set of code here in this class (god help us).
 */
public abstract class CurseForge implements UrlGetter {
    
    private static final Logger LOG = LoggerFactory.getLogger(CurseForge.class);

    /**
     * The addon object we're operating on.
     */
    protected Addon mAddon;

    /**
     * The object to use for communication with CurseForge.
     */
    protected CurseForgeCommunication mComm;

    /**
     * The results from the successful HTTP get on the files URL.
     */
    protected GetMethod mFilesMethod;

    /**
     * Attempts to connect and returns true if successful, false otherwise.
     */
    public boolean connect() throws IOException
    {
        mFilesMethod = mComm.getMethod(getFilesUrl());
        return mFilesMethod != null;
    }


    protected abstract String getBaseUrl();

    protected abstract String getFilesUrl();

    protected abstract String getDownloadSuffix();

    public Optional<String> getDownloadUrl() {
        try {
            String downloadUrl;
            try (BufferedReader reader = mComm.getStreamOverviewSite(mFilesMethod)) {
                downloadUrl = readLines(reader);
            }
            if (downloadUrl.isEmpty()) {
                LOG.debug("Could not find a {} release for {} on {}!", mAddon.getReleaseStatus(), mAddon.getAddonNameId(), getFilesUrl());
                return Optional.empty();
            }
            LOG.debug("downloadUrl: " + downloadUrl);
            return Optional.of(downloadUrl);
        } catch(IOException e) {
            BusinessException be = new BusinessException(String.format("Unable to get download URL for %s!", mAddon.getAddonNameId()), e);
            LOG.error("Exception while getting download URL: {}", be.stackTraceString());
            throw be;
        }

    }

    private String readLines(BufferedReader reader) throws IOException {
        String line;
        release_type:
        while ((line = reader.readLine()) != null)
        {
            if(line.indexOf("release-type") == -1)
                continue release_type;

            title:
            while ((line = reader.readLine()) != null)
            {
                Pattern titlePattern = Pattern.compile("title=\"(.+?)\"");
                Matcher titleMatcher = titlePattern.matcher(line);
                if (!titleMatcher.find())
                    continue release_type;

                try
                {
                    ReleaseStatus releaseStatus = ReleaseStatus.valueOfIgnoreCase(titleMatcher.group(1));
                    if (mAddon.getReleaseStatus().ordinal() > releaseStatus.ordinal())
                        continue release_type;
                } catch (IllegalArgumentException e) {
                    continue release_type;
                }

                href:
                while ((line = reader.readLine()) != null)
                {
                    Pattern hrefPattern = Pattern.compile("href=\"(.+?)\"");
                    Matcher hrefMatcher = hrefPattern.matcher(line);
                    if (!hrefMatcher.find())
                        continue href;

                    String downloadUrl = getBaseUrl() + hrefMatcher.group(1) + getDownloadSuffix();
                    return downloadUrl;
                }
            }

        }
        return "";
    }

    protected CurseForge(Addon addon, CurseForgeCommunication comm)
    {
        mAddon = addon;
        mComm = comm;
    }
}
