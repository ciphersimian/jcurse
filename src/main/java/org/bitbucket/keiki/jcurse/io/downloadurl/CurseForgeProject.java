package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bitbucket.keiki.jcurse.data.Addon;

/**
 * @See CurseForge
 *
 * This subclass provides the mechanisms to parse URLs of the form:
 *
 * http://wow.curseforge.com/projects/<addon>
 */
public class CurseForgeProject extends CurseForge {
    
    private static final Logger LOG = LoggerFactory.getLogger(CurseForgeProject.class);

    public CurseForgeProject(Addon addon, CurseForgeCommunication comm)
    {
        super(addon, comm);
    }

    @Override
    protected String getBaseUrl()
    {
        return mComm.getProjectBaseUrl();
    }

    @Override
    protected String getFilesUrl()
    {
        return mComm.getProjectFilesUrl(mAddon);
    }

    @Override
    protected String getDownloadSuffix()
    {
        // projects links don't need this; the direct download link is in the
        // overview page
        return "";
    }
}
