package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.io.IOException;

import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @See CurseForge
 *
 * This factory class is responsible for constructing the correct type of
 * CurseForge object for the addon.
 */
public class CurseForgeFactory {
    
    private static final Logger LOG = LoggerFactory.getLogger(CurseForgeFactory.class);

    /**
     * Create the appropriate type of CurseForge to process this addon.
     */
    public static CurseForge Create(Addon addon, CurseForgeCommunication comm) {
        try {
            // first try a http://wow.curseforge.com/projects/<addon> URL
            CurseForgeProject project = new CurseForgeProject(addon, comm);
            if(project.connect())
                return project;

            // next try a http://www.curseforge.com/wow/addons/<addon> URL
            CurseForgeWowAddon wowAddon = new CurseForgeWowAddon(addon, comm);
            if(wowAddon.connect())
                return wowAddon;

            throw new IOException(String.format("Unable to find CurseForge page for %s (tried: %s and %s}", addon.getAddonNameId(), comm.getProjectFilesUrl(addon), comm.getWowAddonFilesUrl(addon)));
        } catch (IOException e) {
            BusinessException be = new BusinessException(String.format("Exception during CurseForge::Create: %s", addon.getAddonNameId()), e);
            LOG.debug(be.stackTraceString());
            throw be;
        }
    }

    private CurseForgeFactory() {}
}
