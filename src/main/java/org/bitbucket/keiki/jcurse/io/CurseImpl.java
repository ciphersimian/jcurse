package org.bitbucket.keiki.jcurse.io;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.bitbucket.keiki.jcurse.io.Constants.USER_AGENT;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.bitbucket.keiki.jcurse.data.ReleaseStatus;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForge;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForgeCommunication;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForgeFactory;
import org.bitbucket.keiki.jcurse.io.downloadurl.UrlGetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurseImpl implements Curse {
    
    private static final int NUMBER_OF_THREADS = 10;

    private static final Logger LOG = LoggerFactory.getLogger(CurseImpl.class);
    private AddonFileHandler folderHandler;
    
    public CurseImpl(String addonFolderName) {
        this.folderHandler = new AddonFileHandler(addonFolderName);
    }
    
    protected void downloadAndExtract(Addon newAddon, String downloadUrl, String cookies) {
        try {
            URL website = new URL(downloadUrl);
            URLConnection connection = website.openConnection();
            connection.setRequestProperty("User-Agent", USER_AGENT);
            if(cookies != null)
            {
                connection.setRequestProperty("Cookie", cookies);
            }

            String location = connection.getHeaderField("Location");
            if (isEmpty(location)) {
                // save the name of the actual zip we downloaded
                InputStream is = connection.getInputStream();
                String[] realUrlSplit = StringUtils.split(connection.getURL().toString(), '/');
                String zipFilename = realUrlSplit[realUrlSplit.length - 1];
                newAddon.setLastZipFileName(zipFilename);

                // set the versionId
                String[] split = StringUtils.split(downloadUrl, '/');
                int versionId = extractVersionId(split);
                newAddon.setVersionId(versionId);

                // download the file and save the folder list
                Set<String> addonFolders = folderHandler.download(is);
                newAddon.setFolders(addonFolders);
            } else {
                // get the cookie if need, for login
                String newCookies = connection.getHeaderField("Set-Cookie");                
                downloadAndExtract(newAddon, location, newCookies);
            }

        } catch (IOException e) {
            throw new BusinessException("Problems reading data from Curse." +
                    " If this issue persists please create a bug.", e);
        }
    }

    /**
     * This method has to handle two different URL formats:
     *
     * https://wow.curseforge.com/projects/deadly-boss-mods/files/2728421/download
     * [ "https:", "", "wow.curseforge.com", "projects", "deadly-boss-mods", "files", "2728421", "download" ]
     *
     * OR
     *
     * https://www.curseforge.com/wow/addons/bagnon/download/2690130/file
     * [ "https:", "", "www.curseforge.com", "wow", "addons", "bagnon", "download", "2690130", "file" ]
     *
     * But for now we can treat them both the same because the bit we want is
     * the 2nd to last element in both cases.
     *
     * If this becomes overly cumbersome this logic should just be moved
     * inside the CurseForgeProject/CurseForgeWowAddon classes.
     */
    public static int extractVersionId(String[] split) {
        LOG.debug("extractVersionId: {}", Arrays.toString(split));
        try {
            return Integer.parseInt(split[split.length-2]);
        } catch(NumberFormatException e) {
            throw new BusinessException("Unknown URL Format: " + Arrays.toString(split));
        }
    }

    @Override
    public String getDownloadUrl(Addon addon) {
        CurseForgeCommunication comm = new CurseForgeCommunication();
        UrlGetter curseForge = CurseForgeFactory.Create(addon, comm);

        Optional<String> curseForgeUrl = curseForge.getDownloadUrl();
        if (curseForgeUrl.isPresent()) {
            return curseForgeUrl.get();
        }
        throw new IllegalStateException("No download url found");
    }


    @Override
    public void removeAddons(Collection<Addon> toDelete) {
        for (Addon addon : toDelete) {
            LOG.info("removed {}", addon.getAddonNameId());
            removeAddon(addon);
        }
    }
    
    @Override
    public void removeAddon(Addon toDelete) {
        folderHandler.removeAddonFolders(toDelete.getFolders());
    }

    @Override
    public void downloadToWow(Addon newAddon, String downloadUrl) {
        downloadAndExtract(newAddon, downloadUrl, null);
    }
    
    private boolean downloadToWow(Addon newAddon) {
        try {
            String downloadUrl = getDownloadUrl(newAddon);
            downloadToWow(newAddon, downloadUrl);
            return true;
        } catch (NoSuchElementException e) {
            LOG.warn("No addon found with the name '" + newAddon.getAddonNameId() + "'. Skipping.");
            LOG.debug("No addon found", e);
        }
        return false;
    }

    @Override
    public List<Addon> downloadToWow(List<Addon> toDownload) {
        List<Addon> downloadedAddons = new ArrayList<>();
        List<Callable<Void>> futures = new ArrayList<>(toDownload.size());
        for (final Addon addon : toDownload) {
            futures.add(() -> {
                try
                {
                    if (downloadToWow(addon)) {
                        LOG.info("added {}", addon);
                        downloadedAddons.add(addon);
                    }
                }
                catch(Exception e)
                {
                    BusinessException be = new BusinessException(String.format("Failed to download addon: %s", addon.getAddonNameId()), e);
                    LOG.error(be.stackTraceString());
                }
                return null;
            });
        }
        ExecutorService executerService = Executors.newFixedThreadPool(NUMBER_OF_THREADS); 
        try {
            executerService.invokeAll(futures);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        executerService.shutdown();
        return downloadedAddons;
    }
}
