package org.bitbucket.keiki.jcurse.io.downloadurl;

import static org.bitbucket.keiki.jcurse.io.Constants.CHARSET_WEBSITE;
import static org.bitbucket.keiki.jcurse.io.Constants.USER_AGENT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bitbucket.keiki.jcurse.data.Addon;

/**
 * @See CurseForge
 *
 * This communication class primarily exists as an easy place for mocking and
 * dependency injection.
 */
public class CurseForgeCommunication {
    
    private static final Logger LOG = LoggerFactory.getLogger(CurseForgeCommunication.class);

    public String getProjectBaseUrl()
    {
        return "http://wow.curseforge.com";
    }

    public String getProjectFilesUrl(Addon addon)
    {
        return getProjectBaseUrl() + "/projects/" + addon.getAddonNameId() + "/files";
    }

    public String getWowAddonBaseUrl()
    {
        return "http://www.curseforge.com";
    }

    public String getWowAddonFilesUrl(Addon addon)
    {
        return getWowAddonBaseUrl() + "/wow/addons/" + addon.getAddonNameId() + "/files";
    }

    public GetMethod getMethod(String url) throws IOException
    {
        HttpClient httpClient = new HttpClient();
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("user-agent", USER_AGENT);
        method.setQueryString(new NameValuePair[] { 
            new NameValuePair("sort", "releasetype") 
        });
        int status = executeHttp(httpClient, method);
        LOG.debug("status {} accessing {}", status, url);
        if(status == HttpURLConnection.HTTP_OK)
            return method;
        else
            return null;
    }

    public int executeHttp(HttpClient httpClient, GetMethod method) throws IOException {
        return httpClient.executeMethod(method);
    }

    public BufferedReader getStreamOverviewSite(GetMethod method) throws IOException {
        return new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream(), CHARSET_WEBSITE));
    }

    public BufferedReader getStreamDetailsSite(GetMethod method) throws IOException {
        return new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream(), CHARSET_WEBSITE));
    }
}
