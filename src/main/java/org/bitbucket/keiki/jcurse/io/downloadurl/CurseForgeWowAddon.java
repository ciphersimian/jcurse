package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.io.IOException;

import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bitbucket.keiki.jcurse.data.Addon;

/**
 * @See CurseForge
 *
 * This subclass provides the mechanisms to parse URLs of the form:
 *
 * http://www.curseforge.com/wow/addons/<addon>
 */
public class CurseForgeWowAddon extends CurseForge {
    
    private static final Logger LOG = LoggerFactory.getLogger(CurseForgeWowAddon.class);

    public CurseForgeWowAddon(Addon addon, CurseForgeCommunication comm)
    {
        super(addon, comm);
    }

    @Override
    protected String getBaseUrl()
    {
        return mComm.getWowAddonBaseUrl();
    }

    @Override
    protected String getFilesUrl()
    {
        return mComm.getWowAddonFilesUrl(mAddon);
    }

    @Override
    protected String getDownloadSuffix()
    {
        return "/file";
    }
}
