#!/usr/bin/env bash

# logging
#-Dorg.slf4j.simpleLogger.defaultLogLevel=debug 
java -classpath ./java:$CLASSPATH org.bitbucket.keiki.jcurse.console.Console "$@"
